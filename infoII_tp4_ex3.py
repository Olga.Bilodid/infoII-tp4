#bilodid olga tp4 ex3
# j'avais un probleme avec le commit et push, c'est pourquoi il y a quelques commits pour le meme code

class FilePrioritaire:
    def __init__(self):
        self.items = []

    def enqueue(self, item):
        if self.is_empty():
            self.items.append(item)
        else:
            for i in range(len(self.items)):
                #item[0] -> priorité, items[i][0] -> priorité d'élément numéro i dans self.items
                if item[0] < self.items[i][0]:
                    self.items.insert(i, item)
                    break
            else:
                self.items.append(item)

    def dequeue(self):
        if not self.is_empty():
            return self.items.pop(0)
        else:
            return None

    def is_empty(self):
        return (self.items == [])

    def size(self):
        return len(self.items)

    def afficher(self):
        for item in self.items:
            print(item)

if __name__ == '__main__':
    file = FilePrioritaire()

    file.enqueue((4, "Nathan"))
    file.enqueue((2, "Julia"))
    file.enqueue((1, "Amandine"))

    print("File maintenant: ", file.size())
    file.afficher()

    file.dequeue()
    print("\nFile maintenant: ", file.size())
    file.afficher()

    file.enqueue((3, "Mathias"))
    print("\nFile maintenant: ", file.size())
    file.afficher()







